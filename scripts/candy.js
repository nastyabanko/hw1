import { Dessert } from "./dessert.js";

class Candy extends Dessert {
  constructor(brandName, amountOfSugar, weight, amount, isLiqueur, isNutty ) {
    super(brandName, amountOfSugar, weight, amount);
    this.isLiqueur = isLiqueur;
    this.isNutty = isNutty;
  }
}

export { Candy };