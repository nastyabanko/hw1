import { Dessert } from "./dessert.js";

class Chocolate extends Dessert {
  constructor(brandName, amountOfSugar, weight, amount, isNutty, type, isPorous ) {
    super(brandName, amountOfSugar, weight, amount);
    this.type = type;
    this.isNutty = isNutty;
    this.isPorous = isPorous;
  }
}

export { Chocolate };