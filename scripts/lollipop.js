import { Dessert } from "./dessert.js";

class Lollipop extends Dessert {
  constructor(brandName, amountOfSugar, weight, amount, taste, isOnStick ) {
    super(brandName, amountOfSugar, weight, amount);
    this.taste = taste;
    this.isOnStick = isOnStick;
  }
}

export { Lollipop };