import { Present } from "./present.js";
import { Candy } from "./candy.js";
import { Chocolate } from "./chocolate.js";
import { Lollipop } from "./lollipop.js";
import { Cookie } from "./cookie.js";

const addCandyBtn = document.getElementById("addCandies");
const addChocolateBtn = document.getElementById("addChocolate");
const addCookieBtn = document.getElementById("addCookie");
const addLollipopBtn = document.getElementById("addLollipop");

const generatePresentBtn = document.getElementById("generatePresent");
const showGeneralWeightBtn = document.getElementById("showGeneralWeight");
const sortBrandNameBtn = document.getElementById("sortBrandName");
const sortWeightBtn = document.getElementById("sortWeight");

const cleanBtn = document.getElementById("clean");

const dessertData = document.getElementById("dessertData");
const errorSection = document.getElementById("error");

const allDessert = [];
let freshPresent = null;

const createDessert = (type) => {
  let dessert = null;
  let brandName = prompt("Brand name", "Unknown");
  let amountOfSugar = prompt("Amount of sugar", 1);
  let weight = prompt("Weight (gm)", 1);
  let amount = prompt("How much", 1);
  let testData = brandName && +amountOfSugar > 0 && +weight > 0 && +amount > 0;
  if (type === "candy" && testData) {
    let isLiquor = confirm("Does it have liquor?");
    let isNutty = confirm("Does it have nuts?");
    dessert = new Candy(
      brandName,
      amountOfSugar,
      weight,
      amount,
      isLiquor,
      isNutty
    );
  } else if (type === "chocolate" && testData) {
    let isPorous = confirm("Does it have porous?");
    let isNutty = confirm("Does it have nuts?");
    let chocolateType = prompt("Type of chocolate");
    if (chocolateType) {
      dessert = new Chocolate(
        brandName,
        amountOfSugar,
        weight,
        amount,
        isNutty,
        chocolateType,
        isPorous
      );
    }
  } else if (type === "cookie" && testData) {
    let cookieType = prompt("Type of cookie");
    let cookieTaste = prompt("Taste of cookie");
    if (cookieType && cookieTaste) {
      dessert = new Cookie(
        brandName,
        amountOfSugar,
        weight,
        amount,
        cookieType,
        cookieTaste
      );
    }
  } else if (type === "lollipop" && testData) {
    let lollipopTaste = prompt("Taste of lollipop");
    let isOnStick = confirm("On a stick?");
    if (lollipopTaste) {
      dessert = new Lollipop(
        brandName,
        amountOfSugar,
        weight,
        amount,
        lollipopTaste,
        isOnStick
      );
    }
  }
  return dessert;
};

const saveDessert = (type) => {
  const newDessert = createDessert(type);
  if (newDessert) {
    errorSection.innerHTML = "";
    generatePresentBtn.disabled = false;
    cleanBtn.disabled = false;
    allDessert.push(newDessert);
  } else errorSection.innerHTML = "Something goes wrong";
};

addCandyBtn.addEventListener("click", () => {
  saveDessert("candy");
});
addChocolateBtn.addEventListener("click", () => {
  saveDessert("chocolate");
});
addCookieBtn.addEventListener("click", () => {
  saveDessert("cookie");
});
addLollipopBtn.addEventListener("click", () => {
  saveDessert("lollipop");
});

generatePresentBtn.addEventListener("click", () => {
  freshPresent = new Present([...allDessert]);
  console.log(freshPresent, "freshPresent");
  showGeneralWeightBtn.disabled = false;
  sortBrandNameBtn.disabled = false;
  sortWeightBtn.disabled = false;

  addCandyBtn.disabled = true;
  addChocolateBtn.disabled = true;
  addCookieBtn.disabled = true;
  addLollipopBtn.disabled = true;
});

showGeneralWeightBtn.addEventListener("click", () => {
  if (freshPresent) {
    dessertData.innerText = `General weight: ${freshPresent.showGeneralWeight()}`;
  }
});
sortBrandNameBtn.addEventListener("click", () => {
  if (freshPresent) {
    let brandNames = "";
    freshPresent.sortBrandName().forEach((el) => {
      brandNames += `${el.brandName} `;
    });
    dessertData.innerText = `Sort brand name: ${brandNames}`;
  }
});
sortWeightBtn.addEventListener("click", () => {
  if (freshPresent) {
    const minWeight = prompt("Min weight (gm)", 1);
    const maxWeight = prompt("Max weight (gm)", 500);
    if (+minWeight >= 0 && +maxWeight > 0) {
      let sortedWeight = "";
      freshPresent.sortWeight(minWeight, maxWeight).forEach((el) => {
        sortedWeight += `(${el.brandName} ${el.weight}) `;
      });
      dessertData.innerText = `Sort weight: ${
        sortedWeight ? sortedWeight : "Empty"
      }`;
    } else errorSection.innerHTML = "Something goes wrong";
  }
});

cleanBtn.addEventListener("click", () => {
  allDessert.length = 0;
  freshPresent = null;
  generatePresentBtn.disabled = true;
  showGeneralWeightBtn.disabled = true;
  sortBrandNameBtn.disabled = true;
  sortWeightBtn.disabled = true;
  cleanBtn.disabled = true;

  addCandyBtn.disabled = false;
  addChocolateBtn.disabled = false;
  addCookieBtn.disabled = false;
  addLollipopBtn.disabled = false;

  dessertData.innerText = "";
  errorSection.innerText = "";
});

// const Twix = new Candy("Twix", "30", "50", "1", false, false);
// const Mars = new Candy("Mars", "30", "50", "1", false, false);

// const Milka = new Chocolate("Milka", "50", "100", "2", false, "white", false);

// const ChupaChups = new Lollipop("ChupaChups", "15", "15", "2", "apple", true);

// const Oreo = new Cookie("Oreo", "20", "25", "4", "sugary", "chocolate");

// const firstPresent = new Present([Twix, Mars, Milka, ChupaChups, Oreo]);

// console.log(firstPresent.showGeneralWeight(), "first");
// console.log(firstPresent.sortBrandName(), "sortBrandName");
// console.log(firstPresent.sortWeight("10", "40"), "sortWeight");
