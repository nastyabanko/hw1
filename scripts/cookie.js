import { Dessert } from "./dessert.js";

class Cookie extends Dessert {
  constructor(brandName, amountOfSugar, weight, amount, type, taste ) {
    super(brandName, amountOfSugar, weight, amount);
    this.type = type;
    this.taste = taste;
  }
}

export { Cookie };