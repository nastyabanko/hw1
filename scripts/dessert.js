class Dessert {
    constructor(brandName, amountOfSugar, weight, amount) {
      this.brandName = brandName;
      this.amountOfSugar = amountOfSugar;
      this.weight = weight;
      this.amount = amount;
    }
  }
  
  export { Dessert };