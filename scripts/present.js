class Present {
  constructor(desserts) {
    this.desserts = desserts;
  }

  showGeneralWeight() {
    let result = 0;
    this.desserts.forEach((el) => {
      result += +el.weight * +el.amount;
    });
    return result;
  }

  sortBrandName() {
    return this.desserts.sort((a, b) => (a.brandName > b.brandName ? 1 : -1));
  }

  sortWeight(first, second) {
    return this.desserts.filter(
      (el) => +el.weight >= +first && +el.weight <= +second
    );
  }
}

export { Present };
